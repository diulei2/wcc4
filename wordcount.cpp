/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:www.cplusplus.com
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 5 days in total, few hours per day.
 */

#include <iostream>
#include <string>
#include <set>
#include <sstream>
#include <vector>
using namespace std;

// write this function to help you out with the computation.
unsigned long countwords(const string &s, set<string> &wl) {
    unsigned long count = 0;
    string w;
    stringstream ss(s);

    while (ss >> w) {
        wl.insert(w);
        ++count;
    }
    return count;
}

int main() {
    string line;
    set<string> setofword, setofline;
    unsigned long lcount = 0, words = 0, ccount = 0;


    while (getline(cin, line)) {
        ++lcount;
        ccount += line.size();
        setofline.insert(line);
        words += countwords(line, setofword);
    }
    cout << "\n" << lcount<< "\t " ;
    cout << words<< "\t " ;
    cout << ccount+lcount<< "\t " ;
    cout << setofline.size()<< "\t " ;
    cout <<  setofword.size()<< "\t "  ;
    return 0;
}