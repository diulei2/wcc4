#include <iostream>
#include <string>
#include <sstream>
#include <set>
#include <vector>

using namespace std;

unsigned long countwords(const string &s, set<string> &wl) {
    unsigned long count = 0;
    string word;
    stringstream ss(s);

    while (ss >> word) {
        wl.insert(word);
        ++count;
    }
    return count;
}

int main() {
    string line;
    set<string> wordSet, lineSet;
    unsigned long lines = 0, words = 0, charcount = 0;


    while (getline(cin, line)) {
        ++lines;
        charcount += line.size();
        lineSet.insert(line);
        words += countwords(line, wordSet);
    }
    cout << endl << "\t " << lines;
    cout << "\t " << words;
    cout << "\t " << charcount+lines;
    cout << "\t " << lineSet.size();
    cout << "\t " << wordSet.size() ;
    return 0;
}