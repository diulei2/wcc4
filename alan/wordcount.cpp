/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;

// write this function to help you out with the computation.
// word count
unsigned long countWords(const string& s, set<string>& wl) {
	int wordcount=1;
	char space = ' ';
	for(int i = 0; i < s.length(); i++) {
		if(s[i] == space && s[i+1] != space || s[i] == '\n') {
			wordcount++;
		}	
	}
	cout << wordcount << "\t";
}

unsigned long uniquewords(const string& s, set<string>& wl) {
}

int main()
{
	string s;
	set<string> wl;
	string line;
	int wordcount;
	int numberofline = 0;
	while(getline(cin, line)) {
		s += line;
		wl.insert(line);
		numberofline ++;
		cout << numberofline << "\t"; //number of line
		countWords(s, wl); //number of word
		cout << s.length() << "\t"; //number of character
		cout << wl.size() << "\t"; //number of unique lines
	}
	
	return 0;
}
